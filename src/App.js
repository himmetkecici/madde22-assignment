import React from 'react';
import './styles/app.scss';

import Navbar from './components/Navbar';
import Search from './components/Search';
import HeaderIcons from './components/HeaderIcons';
import Profile from './components/Profile';
import Box from './components/Box';
import SmartPlanningBox from './components/SmartPlanningBox';
import RightPicBox from './components/RightPicBox';
import CalendarBox from './components/CalendarBox';
import GoalsBox from './components/GoalsBox';
import SmartProgressBox from './components/SmartProgressBox';
import Welcome from './components/Welcome';

function App() {
  return (
    <div className="app">
      <Navbar />
      <div className="page">
        <header>
          <Search />
          <HeaderIcons />
          <aside>
            <Profile />
          </aside>
        </header>
        <main>
          <article>
            <SmartPlanningBox />
            <CalendarBox />
            <Box className="no-bg no-padding no-margin flex-row">
              <GoalsBox />
              <SmartProgressBox />
            </Box>
          </article>
          <aside>
            <RightPicBox />
            <Welcome />
          </aside>
        </main>
      </div>
    </div>
  );
}

export default App;

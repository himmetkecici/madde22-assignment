import React, { useState } from 'react';
import '../styles/components/tabs.scss';

const Tabs = ({ tabs }) => {
  const [selected, setSelected] = useState(0);

  return (
    <div className="flex-row tabs">
      {tabs.map((tab, index) => (
        <button
          key={index}
          onClick={() => setSelected(index)}
          className={`tab ${selected === index ? 'tab--selected' : ''}`}
        >
          {tab}
        </button>
      ))}
    </div>
  );
};

export default Tabs;

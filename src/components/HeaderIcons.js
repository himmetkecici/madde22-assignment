import React from 'react';
import { mdiBellOutline } from '@mdi/js';
import '../styles/components/header-icons.scss'

import Box from './Box';
import IconButton from './IconButton';

const HeaderIcons = () => (
  <Box className='header__header-icons no-bg no-padding'>
    <IconButton path={mdiBellOutline} badge />
  </Box>
)

export default HeaderIcons;
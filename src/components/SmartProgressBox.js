import React from 'react';
import '../styles/components/smart-progress-box.scss';

import Box from './Box';
import ProgressBar from './ProgressBar';
import Tabs from './Tabs';

const progressPercent = 45.6;

const SmartProgressBox = () => (
  <Box className="flex-1 bg-dark smart-progress-box">
    <div className="flex-column" style={{ paddingLeft: 120 }}>
      <h3>SMART Progress</h3>
      <span>{progressPercent}%</span>
      <ProgressBar percent={progressPercent} />
      <Tabs tabs={['Archived', 'Active']} />
    </div>
    <img src="human-2.png" alt="" style={{ top: -25, left: 17 }} />
  </Box>
);

export default SmartProgressBox;

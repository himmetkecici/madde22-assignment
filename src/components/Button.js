import React from 'react';
import classNames from 'classnames';
import '../styles/components/button.scss';

const Button = ({ className, big, outline, ...props }) => (
  <button
    className={classNames('box no-margin no-padding flex-row button', className, {
      'button--big': big,
      'button--outline': outline,
    })}
    {...props}
  />
);

export default Button;

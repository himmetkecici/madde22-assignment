import React from 'react';
import Box from './Box';

const RightPicBox = () => (
  <Box
    className="bg-yellow"
    style={{
      backgroundImage: 'url(right-pic.png)',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center center',
      height: 400,
    }}
  />
);

export default RightPicBox;

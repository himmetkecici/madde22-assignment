import React from 'react';
import '../styles/components/button-group.scss';

import Box from './Box';

const ButtonGroup = ({ children, ...props }) => (
  <div className="button-group" {...props}>
    <Box className="flex-row no-padding no-margin-y">
      {children}
    </Box>
  </div>
)

export default ButtonGroup;

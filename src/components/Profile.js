import React from 'react';
import '../styles/components/profile.scss';

import Box from './Box';

const Profile = () => (
  <Box className="no-bg no-padding">
    <a href="#profile" className="profile">
      <img className="profile__photo" src="profile.jpg" alt="Profile" />
      Himmet Keçici
    </a>
  </Box>
);

export default Profile;

import React from 'react';
import { mdiChartArc, mdiCloudOutline } from '@mdi/js';
import '../styles/components/smart-planning-box.scss';

import Box from './Box';
import IconButton from './IconButton';

const SmartPlanningBox = () => (
  <Box className="flex-row smart-planning-box">
    <Box className="flex-1 flex-column no-bg no-padding no-margin">
      <h1>What is SMART Planning?</h1>
      <div className="description">
        Business goals should always be Specific, Measurable and Achievable.
      </div>
      <a href="#test">{'learn more >'}</a>
    </Box>
    <IconButton className="box flex-column bg-orange" path={mdiChartArc}>
      Graph
    </IconButton>
    <IconButton className="box flex-column bg-blue" path={mdiCloudOutline}>
      Cloud
    </IconButton>
  </Box>
);

export default SmartPlanningBox;

import React from 'react';
import '../styles/components/box.scss';

const Box = ({ className = '', ...props }) => (
  <div className={`box ${className}`} {...props} />
)

export default Box;

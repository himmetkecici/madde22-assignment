import React, { useState } from 'react';
import { mdiChevronLeft, mdiChevronRight, mdiChevronDown } from '@mdi/js';
import { addDays, startOfWeek, format, subDays } from 'date-fns';
import '../styles/components/calendar-box.scss';

import Box from './Box';
import IconButton from './IconButton';
import ButtonGroup from './ButtonGroup';
import Button from './Button';
import Icon from '@mdi/react';
import CalendarSelect from './CalendarSelect';

const getThisWeek = (today) => {
  const monday = startOfWeek(today, { weekStartsOn: 1 });
  const weekDays = [];

  for (let i = 0; i < 7; i++) {
    weekDays.push(addDays(monday, i));
  }

  return weekDays;
}

const renderWeekName = (weekDays) => {
  const monday = weekDays[0];
  const sunday = weekDays[6];

  return `${format(monday, 'd')} - ${format(sunday, 'd')} ${format(sunday, 'MMMM')}`
}

const CalendarBox = () => {
  const today = new Date();
  const [weekDays, setWeekDays] = useState(getThisWeek(today));
  const [selectedDay, setSelectedDay] = useState(today);

  const goToNextWeek = () => {
    const nextWeek = weekDays.map(day => addDays(day, 7));
    setWeekDays(nextWeek);
  }

  const goToPreviousWeek = () => {
    const previousWeek = weekDays.map(day => subDays(day, 7));
    setWeekDays(previousWeek);
  }

  const onSelectDay = (day) => {
    setSelectedDay(day);
  }

  return (
    <Box className='calendar-box no-bg no-padding-x'>
      <div className="flex-row justify-space-between" style={{ paddingLeft: 26 }}>
        <div className="flex-row">
          <Box className="no-bg no-margin no-padding calendar-box__title">
            <strong>{renderWeekName(weekDays)}</strong>
          </Box>
          <ButtonGroup>
            <IconButton className="bg-orange" path={mdiChevronLeft} onClick={goToNextWeek} />
            <IconButton className="bg-orange" path={mdiChevronRight} onClick={goToPreviousWeek} />
          </ButtonGroup>
        </div>
        <Button outline>
          Week
          <Icon path={mdiChevronDown} />
        </Button>
      </div>

      <CalendarSelect days={weekDays} selectedDay={selectedDay} onSelectDay={onSelectDay}  />
    </Box>
  )
}

export default CalendarBox;

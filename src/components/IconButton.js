import React from 'react';
import Icon from '@mdi/react';
import '../styles/components/icon-button.scss';

const IconButton = ({ path, className, iconOptions, badge, children, ...props }) => (
  <button className={`icon-button ${className} ${badge ? 'icon-button--badge' : ''}`} {...props}>
    <Icon path={path} {...iconOptions} />
    {children}
  </button>
)

export default IconButton;
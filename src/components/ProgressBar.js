import React from 'react';
import '../styles/components/progress-bar.scss';

const ProgressBar = ({ percent }) => (
  <div className="flex-1 flex-row progress-bar">
    <div className='progress-bar__indicator' style={{ width: `${percent}%` }} />
  </div>
)

export default ProgressBar;

import React from 'react';
import { format, isSameDay } from 'date-fns';
import '../styles/components/calendar-select.scss';

import Button from './Button';

const CalendarSelect = ({ days, selectedDay, onSelectDay }) => (
  <div className="flex-row calendar-select">
    {days.map(day => (
      <Button
        key={day}
        className={`no-bg calendar-select__day-button ${
          isSameDay(selectedDay, day) ? 'calendar-select__day-button--selected' : ''
        }`}
        onClick={() => onSelectDay(day)}
      >
        <div className="flex-column">
          <div className="calendar-select__day-name">{format(day, 'EEEEEE')}</div>
          <div className="calendar-select__day-number">{format(day, 'd')}</div>
        </div>
      </Button>
    ))}
    <Button outline className="calendar-select__in-progress-button">
      In Progress
    </Button>
  </div>
);

export default CalendarSelect;

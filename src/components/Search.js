import React, { useState } from 'react';
import { mdiMagnify } from '@mdi/js';
import '../styles/components/search.scss';

import Box from './Box';
import IconButton from './IconButton';

const SearchForm = () => {
  const [value, setValue] = useState('');

  const onSubmit = event => {
    event.preventDefault();
    event.stopPropagation();

    alert(`Search: ${value}`);
  }

  return (
    <Box className="search no-padding">
      <form onSubmit={onSubmit}>
        <IconButton type="submit" path={mdiMagnify} />
        <input
          type="search"
          name="search"
          value={value}
          onChange={event => setValue(event.target.value)}
          className="search-input"
          placeholder="Search"
        />
      </form>
    </Box>
  )
}

export default SearchForm;

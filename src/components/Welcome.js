import React from 'react';
import '../styles/components/welcome.scss';

import Box from './Box';
import Button from './Button';

const Welcome = () => (
  <Box className="no-bg no-padding-x welcome">
    <h2>Welcome to SMART</h2>
    Goal setting amd goal management software for high achievers.
    <Button big className="bg-orange">Get Started</Button>
  </Box>
)

export default Welcome;

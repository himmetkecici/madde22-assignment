import React from 'react';
import Icon from '@mdi/react';
import { mdiEmailOutline, mdiExport, mdiClockOutline, mdiCalendarBlankOutline } from '@mdi/js';
import '../styles/components/navbar.scss';

const menuItems = [
  { link: '#home', title: 'Home', current: true },
  { link: '#mails', title: 'Mails', icon: mdiEmailOutline },
  { link: '#time-tracking', title: 'Time Tracking', icon: mdiClockOutline },
  { link: '#calendar', title: 'Calendar', icon: mdiCalendarBlankOutline },
];

const Navbar = () => {
  return (
    <div className="navbar">
      <div className="navbar__item navbar__item--logo">
        <img className="navbar__item__logo" src="logo.svg" alt="Logo" />
      </div>

      <nav>
        {menuItems.map(item => (
          <li key={item.link}>
            <a href={item.link} title={item.title} className={item.current ? 'current' : ''}>
              {!item.icon ? item.title : <Icon path={item.icon} />}
            </a>
          </li>
        ))}
      </nav>

      <div className="navbar__item">
        <Icon path={mdiExport} size="26px" />
      </div>
    </div>
  );
};

export default Navbar;

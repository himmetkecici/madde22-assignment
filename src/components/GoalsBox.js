import React from 'react';
import Icon from '@mdi/react';
import { mdiCheckBold } from '@mdi/js';
import '../styles/components/goal-box.scss';

import Box from './Box';

const GoalsBox = () => (
  <Box className="flex-1 goal-box">
    <h3>Goals achieved</h3>
    <ul>
      <li>
        <Icon path={mdiCheckBold} />
        Market share
      </li>
      <li>
        <Icon path={mdiCheckBold} />
        Customer service
      </li>
      <li>
        <Icon path={mdiCheckBold} />
        Increase efficiency
      </li>
    </ul>
    <img src="human-1.png" alt="" style={{ top: -27, right: 24 }} />
  </Box>
);

export default GoalsBox;
